package com.example.trafficlight;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.provider.CalendarContract;
import android.text.Layout;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button btnRed;
    Button btnYellow;
    Button btnGreen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public void onClickRed(View view) {
        ConstraintLayout LayoutBackground=(ConstraintLayout) findViewById(R.id.Layout);
        LayoutBackground.setBackgroundColor(Color.RED);
    }
    public void onClickGreen(View view) {
        ConstraintLayout LayoutBackground=(ConstraintLayout) findViewById(R.id.Layout);
        LayoutBackground.setBackgroundColor(Color.GREEN);
    }
    public void onClickYellow(View view) {
        ConstraintLayout LayoutBackground=(ConstraintLayout) findViewById(R.id.Layout);
        LayoutBackground.setBackgroundColor(Color.YELLOW);
    }
}
